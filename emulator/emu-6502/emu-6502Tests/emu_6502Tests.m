//
//  emu_6502Tests.m
//  emu-6502Tests
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface emu_6502Tests : XCTestCase

@end

@implementation emu_6502Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
