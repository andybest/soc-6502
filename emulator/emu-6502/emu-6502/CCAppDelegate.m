//
//  CCAppDelegate.m
//  emu-6502
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import "CCAppDelegate.h"
#import "CCEmuDisplayWindowController.h"

@interface CCAppDelegate ()
@property (nonatomic, strong) CCEmuDisplayWindowController *emuDisplayWindowController;
@end

@implementation CCAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    self.emulatorCore = [[CC6502EmulatorCore alloc] init];
    
    // Insert code here to initialize your application
    self.emuDisplayWindowController = [[CCEmuDisplayWindowController alloc] initWithWindowNibName:@"CCEmuDisplayWindow"];
    [_emuDisplayWindowController showWindow:self];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

@end
