//
//  CCEmulatorCoreBase.h
//  emu-6502
//
//  Created by Andy Best on 11/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CCEmulatorCoreBase <NSObject>
@required

- (void)loadFile:(NSString *)filePath toAddress:(uint16_t)address;
- (void)startEmulationThreadWithCyclesPerSecond:(NSUInteger)cyclesPerSecond;
- (void)stopEmulationThread;
- (NSData*)currentTextMemory;
@end
