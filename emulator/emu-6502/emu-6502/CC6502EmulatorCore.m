//
//  CC6502EmulatorCore.m
//  emu-6502
//
//  Created by Andy Best on 11/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import "CC6502EmulatorCore.h"

@interface CC6502EmulatorCore ()
@property (nonatomic, strong) NSThread *emulationThread;
@end

@implementation CC6502EmulatorCore

- (id)init
{
    if((self = [super init]))
    {
        [self loadFile:@"/Users/andybest/dev/verilog/soc-6502/testASM/hello_txt.obx" toAddress:0x1000];
        [self resetAndJumpToAddress:0x1000];
        [self startEmulationThreadWithCyclesPerSecond:100];
    }
    return self;
}

#pragma mark - Emulation Functions

- (void)reset
{
    reset6502();
}

- (void)resetAndJumpToAddress:(uint16_t)address
{
    _mainMemory6502[0xFFFC] = address & 0xFF;
    _mainMemory6502[0xFFFD] = address >> 8;
    [self reset];
}

- (void)nmi
{
    nmi6502();
}

- (void)irq
{
    irq6502();
}

- (void)emulateCPU:(uint32_t)numCycles
{
    exec6502(numCycles);
}

- (void)singleStepCPU
{
    step6502();
}

- (void)wipeMemory
{
    memset(_mainMemory6502, 0x0, kCC6502MemLength);
}

- (NSData *)currentTextMemory
{
    // Text memory is MEM_TOP - 1000h
    uint8_t *textMemBase = _mainMemory6502 + (kCC6502MemLength - 0x1000);
    NSData *textMem =  [NSData dataWithBytes:textMemBase length:80 * 40];
    return textMem;
}

- (void)loadFile:(NSString *)filePath toAddress:(uint16_t)address
{
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    uint8_t *fileBytes = (uint8_t *)fileData.bytes;
    
    NSInteger dataLength = fileData.length;
    
    // Truncate to end of memory
    if(dataLength + address > kCC6502MemLength)
    {
        dataLength = kCC6502MemLength - address;
    }
    
    memcpy(_mainMemory6502 + address, fileBytes, sizeof(uint8_t) * dataLength);
}

#pragma mark - Emulation Threading

- (void)startEmulationThreadWithCyclesPerSecond:(NSUInteger)cyclesPerSecond
{
    self.emulationThread = [[NSThread alloc] initWithTarget:self
                                                   selector:@selector(emulationLoop:)
                                                     object:@(cyclesPerSecond)];
    [_emulationThread start];
}

- (void)stopEmulationThread
{
    [self.emulationThread cancel];
}

- (void)emulationLoop:(NSNumber *)cyclesPerSecond
{
    @autoreleasepool {
        // Emulation is performed in bursts of 1/60 of a second.
        NSTimeInterval emulationPeriod = 1.0 / 60.0;
        
        while(![_emulationThread isCancelled])
        {
            NSDate *start = [NSDate date] ;
            [self emulateCPU:(uint32_t)([cyclesPerSecond integerValue] * emulationPeriod)];
            [NSThread sleepUntilDate:[NSDate dateWithTimeInterval:emulationPeriod
                                                        sinceDate:start]];
        }
    }
}

@end


uint8_t read6502(uint16_t address)
{
    return _mainMemory6502[address];
}

void write6502(uint16_t address, uint8_t value)
{
    _mainMemory6502[address] = value;
}
