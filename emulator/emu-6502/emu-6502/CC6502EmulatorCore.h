//
//  CC6502EmulatorCore.h
//  emu-6502
//
//  Created by Andy Best on 11/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCEmulatorCoreBase.h"
#import "core6502.h"

#define kCC6502MemLength 0x10000

extern void reset6502();
extern void exec6502(uint32_t tickcount);
extern void step6502();
extern void nmi6502();
extern void irq6502();

uint8_t read6502(uint16_t address);
void write6502(uint16_t address, uint8_t value);

uint8_t _mainMemory6502[kCC6502MemLength];

@interface CC6502EmulatorCore : NSObject <CCEmulatorCoreBase>
{
}


@end
