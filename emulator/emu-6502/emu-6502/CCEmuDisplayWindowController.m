//
//  CCEmuDisplayWindowController.m
//  emu-6502
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import "CCEmuDisplayWindowController.h"
#import "CCAppDelegate.h"
#import "CCEmulatorCoreBase.h"

@interface CCEmuDisplayWindowController ()

@end

@implementation CCEmuDisplayWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
        _dataHash = 0;
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    // Lock aspect ratio
    self.window.aspectRatio = self.window.frame.size;
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &displayLinkCallback, (__bridge void *)(self));
    CVDisplayLinkStart(displayLink);
}

- (void)dealloc
{
    CVDisplayLinkStop(displayLink);
}

static CVReturn displayLinkCallback(CVDisplayLinkRef displayLink,
                                    const CVTimeStamp* now,
                                    const CVTimeStamp* outputTime,
                                    CVOptionFlags flagsIn,
                                    CVOptionFlags* flagsOut,
                                    void* displayLinkContext)
{
    @autoreleasepool {
        CCAppDelegate *appDelegate = (CCAppDelegate *)[[NSApplication sharedApplication] delegate];
        NSData *videoMemory = [appDelegate.emulatorCore currentTextMemory];
        
        CCEmuDisplayWindowController *winController = (__bridge CCEmuDisplayWindowController *)displayLinkContext;
        
        winController.emuDisplayView.textData = videoMemory;
        
        if(winController.dataHash != [videoMemory hash])
        {
            [winController.emuDisplayView setNeedsDisplay:YES];
        }
        
        winController.dataHash = [videoMemory hash];
        
        return kCVReturnSuccess;
    }
}

@end
