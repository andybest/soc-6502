//
//  CCEmuDisplayWindowController.h
//  emu-6502
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreVideo/CoreVideo.h>

#import "CCEmuDisplayView.h"

@interface CCEmuDisplayWindowController : NSWindowController
{
    CVDisplayLinkRef displayLink;
}

@property (nonatomic, weak) IBOutlet CCEmuDisplayView *emuDisplayView;
@property (nonatomic) NSUInteger dataHash;

static CVReturn displayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* displayLinkContext);

@end
