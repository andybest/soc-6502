//
//  main.m
//  emu-6502
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
