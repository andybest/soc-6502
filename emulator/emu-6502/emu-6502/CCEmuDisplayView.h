//
//  CCEmuDisplayView.h
//  emu-6502
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CCEmuDisplayView : NSView

@property (nonatomic, strong) NSData *textData;

@end
