//
//  CC68kEmulatorCore.h
//  emu-6502
//
//  Created by Andy Best on 17/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCEmulatorCoreBase.h"
#import "v68k/emulator.hh"

#define kCC68000MemLength 0x100000

@interface CC68kEmulatorCore : NSObject <CCEmulatorCoreBase>
{
    v68k::emulator *emulatorCore;
    v68k::memory_region *memory;
    v68k::bkpt_handlers *breakpointHandlers;
    uint8_t memoryBytes[kCC68000MemLength];
}

@end
