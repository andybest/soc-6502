//
//  CCAppDelegate.h
//  emu-6502
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CC6502EmulatorCore.h"

@interface CCAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@property (nonatomic, strong) CC6502EmulatorCore *emulatorCore;

@end
