//
//  CCEmuDisplayView.m
//  emu-6502
//
//  Created by Andy Best on 10/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import "CCEmuDisplayView.h"

@interface CCEmuDisplayView ()
@property(nonatomic, strong) NSImage *fontImage;
@property(nonatomic, strong) NSMutableArray *fontImages;
@end

@implementation CCEmuDisplayView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.fontImage = [NSImage imageNamed:@"font_inverted.png"];
    }
    
    return self;
}

- (BOOL)isFlipped
{
    return YES;
}

- (void)drawAsciiChar:(char)character atPoint:(CGPoint)point
{
    CGFloat x, y;
    y = floor(character / 16);
    x = character % 16;
    x *= 8;
    y *= 16;
    
    CGRect charRect = CGRectMake(x, y, 8, 16);
    [_fontImage setFlipped:YES];
    [_fontImage drawAtPoint:point fromRect:charRect operation:NSCompositeCopy fraction:1.0];
}

- (void)drawTextArray:(char *)text
{
    CGFloat x = 0;
    CGFloat y = 0;
    for(int i=0; i < (80 * 40); i++)
    {
        CGPoint point = CGPointMake(x, y);
        [self drawAsciiChar:text[i] atPoint:point];
        
        x += 8;
        if(x >= 640)
        {
            x = 0;
            y += 16;
        }
    }
}

- (void)drawRect:(NSRect)dirtyRect
{
    [[NSColor blackColor] set];
    [NSBezierPath fillRect:dirtyRect];
    
    if(_textData)
    {
        char *text = (char *)_textData.bytes;
        [self drawTextArray:text];
    }
}

@end
