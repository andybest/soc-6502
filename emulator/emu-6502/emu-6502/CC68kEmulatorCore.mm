//
//  CC68kEmulatorCore.m
//  emu-6502
//
//  Created by Andy Best on 17/07/2013.
//  Copyright (c) 2013 Andy Best. All rights reserved.
//

#import "CC68kEmulatorCore.h"
//#import "v68k/state.hh"

@interface CC68kEmulatorCore ()
@property (nonatomic, strong) NSThread *emulationThread;
@end

@implementation CC68kEmulatorCore

- (id)init
{
    if((self = [super init]))
    {
        breakpointHandlers = new v68k::bkpt_handlers
        {
            {
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            }
        };
        
        // 1 MiB of RAM
        memory = new v68k::memory_region(memoryBytes, kCC68000MemLength);
        emulatorCore = new v68k::emulator(v68k::mc68000, *memory, *breakpointHandlers);
        
    }
    return self;
}

- (void)emulateCPU:(uint32_t)numCycles
{
    for(int i = 0; i < numCycles; i++)
    {
        emulatorCore->step();
    }
}

- (void)reset
{
    emulatorCore->reset();
}

- (void)singleStepCPU
{
    emulatorCore->step();
}

- (void)loadFile:(NSString *)filePath toAddress:(uint16_t)address
{
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    uint8_t *fileBytes = (uint8_t *)fileData.bytes;
    
    NSInteger dataLength = fileData.length;
    
    // Truncate to end of memory
    if(dataLength + address > kCC68000MemLength)
    {
        dataLength = kCC68000MemLength - address;
    }
    
    memcpy(memoryBytes + address, fileBytes, sizeof(uint8_t) * dataLength);
}

- (NSData *)currentTextMemory
{
    // Text memory is MEM_TOP - 1000h
    uint8_t *textMemBase = memoryBytes + (kCC68000MemLength - 0x1000);
    NSData *textMem =  [NSData dataWithBytes:textMemBase length:80 * 40];
    return textMem;
}

- (void)startEmulationThreadWithCyclesPerSecond:(NSUInteger)cyclesPerSecond
{
    self.emulationThread = [[NSThread alloc] initWithTarget:self
                                                   selector:@selector(emulationLoop:)
                                                     object:@(cyclesPerSecond)];
    [_emulationThread start];
}

- (void)stopEmulationThread
{
    [self.emulationThread cancel];
}

- (void)emulationLoop:(NSNumber *)cyclesPerSecond
{
    @autoreleasepool {
        // Emulation is performed in bursts of 1/60 of a second.
        NSTimeInterval emulationPeriod = 1.0 / 60.0;
        
        while(![_emulationThread isCancelled])
        {
            NSDate *start = [NSDate date] ;
            [self emulateCPU:(uint32_t)([cyclesPerSecond integerValue] * emulationPeriod)];
            [NSThread sleepUntilDate:[NSDate dateWithTimeInterval:emulationPeriod
                                                        sinceDate:start]];
        }
    }
}


@end
